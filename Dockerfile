FROM golang:1.10 as builder

WORKDIR /go/src/app
COPY . .

RUN go get -d
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app

# Final image.
FROM alpine
COPY --from=builder /go/src/app/app .
COPY --from=builder /go/src/app/static ./static
EXPOSE 8080
ENTRYPOINT ["/app"]