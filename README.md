# Install prerequisites

## Infraestrutura

![alt text](doc/infra.jpg)

A solução funciona da seguinte forma:
 1. Os arquivos são enviados via upload pela API.
 1. A API salva esses arquivos em um volume compartilhado.
 1. A API envia uma mensagem contendo o nome do arquivo.
 1. Os workers recebem a mensagem e processa o arquivo que está no volume.
 1. Os workers salvam o resultado no redis.
 1. A API faz a busca no redis.

## Background

Toda a solução está configurada em docker-compose.yml por simplicidade e é composta por:

 1. Redis
 1. RabbitMQ
 1. API - go-word-count
 1. Workers - go-word-count-worker


## Setup

Instale o Docker com compose.

 1. [Install Docker Compose](https://docs.docker.com/compose/install/)
    * On Linux, follow the instructions over at  https://docs.docker.com/compose/install/
    * On Mac, use [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
    * On Windows, get [Docker for Windows](https://docs.docker.com/docker-for-windows/install/)

 1. Baixe os fontes
    * git clone https://gitlab.com/emersonmatsumoto/go-word-count.git
    * git clone https://gitlab.com/emersonmatsumoto/go-word-count-worker.git

## Iniciando o Word Count

```shell
cd go-word-count
docker-compose up -d
```

### Acesso da API

A API estará rodando na porta 8080

Visit [http://localhost:8080](http://localhost:8080)



### Escalando workers

Utilize o comando abaixo para aumentar ou diminuir a quantidade de workers.

```shell
docker-compose up -d --scale workers=3
```

### Parando a solução

Para parar a solução utilize:

```shell
docker-compose stop
```

Para remover a solução utilize:

```shell
docker-compose rm -f
```

## CI/CD

Detalhes em .gitlab-ci.yml.

### Build

Cada aplicação contém um Dockerfile que compila os fontes e cria um container mínimo com o programa executável. 

Cada push dispara um build que gera uma imagem com duas tags: {commitId} e latest

### Deploy

Via ssh o Runner:
 1. copia arquivo docker-compose.autodeploy.yml
 1. remove o serviço
 1. atualiza a imagem do serviço
 1. sobe novamente o compose


