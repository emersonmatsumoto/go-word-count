package main

import (
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

var redisClient *redis.Client

// Give us some seed data
func init() {
	addr := os.Getenv("REDIS")

	if addr == "" {
		addr = ":6379"
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:         addr,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
	})
}

func RepoFlush() {
	redisClient.FlushDB()
}

func RepoFindWord(key string) (Word, error) {

	val, err := redisClient.Get(key).Result()

	if err == redis.Nil {
		return Word{Key: key, Count: 0}, nil
	}

	i, err := strconv.Atoi(val)

	return Word{Key: key, Count: i}, err
}

func RepoCreateWord(t Word) (Word, error) {

	err := redisClient.Set(t.Key, t.Count, 0).Err()

	return t, err
}

func RepoDestroyWord(key string) error {

	err := redisClient.Del(key).Err()

	return err
}
