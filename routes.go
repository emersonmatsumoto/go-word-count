package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"WordShow",
		"GET",
		"/words/{key}",
		WordShow,
	},
	Route{
		"WordCreate",
		"POST",
		"/words",
		WordCreate,
	},
	Route{
		"WordDelete",
		"DELETE",
		"/words/{key}",
		WordDelete,
	},
	Route{
		"UploadFile",
		"POST",
		"/upload",
		ReceiveFile,
	},
	Route{
		"Flush",
		"POST",
		"/flush",
		Flush,
	},
}
