package main

import (
	"time"
)

type Word struct {
	Key      string        `json:"key"`
	Count    int           `json:"count"`
	Duration time.Duration `json:"duration"`
}
